//#pragma once

#ifndef MYLITTLEHELPERS_H
#define MYLITTLEHELPERS_H

#include <windows.h> //����� ��� ����� ���������

class ConsoleCP
{
    int oldin;
    int oldout;

public:
    ConsoleCP(int cp)
    {
        oldin = GetConsoleCP();
        oldout = GetConsoleOutputCP();
        SetConsoleCP(cp);
        SetConsoleOutputCP(cp);
    }

    // ��������� �� �������� �������� �������� ������� � �������, ��� �����
    // ������� �� ��� ���� (���� ��������� �������, ������������ �� �������)
    ~ConsoleCP()
    {
        SetConsoleCP(oldin);
        SetConsoleOutputCP(oldout);
    }
};

#endif